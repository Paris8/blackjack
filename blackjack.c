/* -- PROJET DE FIN DE SEMESTRE --
   Université Paris 8
   Mars - Avril 2021
   L1Z - Informatique - Département STN
 
   Ce projet a été développé dans le cadre de l'UE "Outils informatiques collaboratifs" avec Mme. Françoise Balmas
 
   Anri Kennel - Felipe Pereira Manfrin - Kévin Martins Da Veiga - William Nizet */

#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32 // si windows
    #include <conio.h>
#else // sinon
    #include <curses.h> // fonction de conio.h en linux
    #include <unistd.h> // fonction sleep
    #include <time.h> // fonction time
#endif


//on utilise une int array pour stocker toutes les cartes du jeu
//au fur et à mesure que les cartes sont distribuées, on enlève ces-là du tableau
int cards[53] = {1,2,3,4,5,6,7,8,9,10,11,12,13,
                14,15,16,17,18,19,20,21,22,23,24,25,26,
                27,28,29,30,31,32,33,34,35,36,37,38,39,
                40,41,42,43,44,45,46,47,48,49,50,51,52};


//chaque joueur ainsi que le croupier sont représentés par une int array
//chaque nouvelle carte reçue est additionnée à la liste correspondante
int player1[10];
int player2[10];
int dealer[10];


//les fonctions suivantes servent à vérifier le système d'exploitation de l'utilisateur
//les intéractions sont légèrement différentes à dépendre du système
int isWindows() {
    #ifdef _WIN32
        return 0;
    #else
        return 1;
    #endif
}

int waitUser() {
    if(isWindows()) {
        printf("Press ENTER to continue...\n\n");
        getchar();
    } else {
        printf("Press any key to continue...\n\n");
        getch();
    }

    return 0;
}

int clearUserScreen() {
    if(isWindows()) system("clear"); else system("cls");

    return 0;
}


//pour avoir la valeur réelle d'une carte, on utilise la fonction real_value (toutes cartes valent quelque chose entre 1 et 13)
//dans notre version du jeu, l'As vaut toujours 1 point et les cartes J,Q,K valent toutes 10 points chaque
int real_value(int nb) {
    if (nb == 1 || nb == 14 || nb == 27 || nb == 40) {
        return 1;
    } else if (nb == 2 || nb == 15 || nb == 28 || nb == 41) {
        return 2;
    } else if (nb == 3 || nb == 16 || nb == 29 || nb == 42) {
        return 3;
    } else if (nb == 4 || nb == 17 || nb == 30 || nb == 43) {
        return 4;
    } else if (nb == 5 || nb == 18 || nb == 31 || nb == 44) {
        return 5;
    } else if (nb == 6 || nb == 19 || nb == 32 || nb == 45) {
        return 6;
    } else if (nb == 7 || nb == 20 || nb == 33 || nb == 46) {
        return 7;
    } else if (nb == 8 || nb == 21 || nb == 34 || nb == 47) {
        return 8;
    } else if (nb == 9 || nb == 22 || nb == 35 || nb == 48) {
        return 9;
    } else if (nb == 10 || nb == 23 || nb == 36 || nb == 49) {
        return 10;
    } else if (nb == 11 || nb == 24 || nb == 37 || nb == 50) {
        return 10;
    } else if (nb == 12 || nb == 25 || nb == 38 || nb == 51) {
        return 10;
    } else if (nb == 13 || nb == 26 || nb == 39 || nb == 52) {
        return 10;
    }

    return 0;
}

//cette fonction sort la couleur de la carte
//on utilise des charactères génériques pour chaque couleur -> '*', '#', '&', '$'
char real_color(int nb) {
    if (nb >= 1 && nb <= 13) {
        return '*';
    } else if (nb >= 14 && nb <= 26) {
        return '#';
    } else if (nb >= 27 && nb <= 39) {
        return '&';
    } else {
        return '$';
    }
}

//cette fonction vérifie la somme cumullé de la personne
//si la somme est <= 21, le joueur peut décider de continuer à tourner des cartes ou pas
//si la somme est > 21, le joueur a perdu automatiquement
int victory(int sum) {
    if (sum <= 21) {
        return 0;
    } else if (sum > 21) {
        return 1;
    }

    return 0;
}

//la fonction suivante dessine la carte
//elle prend la valeur de la carte et sa couleur
//des modifications ont été faites pour l'affichages des As,Js,Qs et Ks.
void draw_card(int value, char couleur) {
    if (value == 1 || value == 14 || value == 27 || value == 40) {
        printf(" -----------\n");
    printf("| A %c       |\n",couleur);
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|        A %c|\n",couleur);
    printf(" -----------\n\n");
    } else if (value == 11 || value == 24 || value == 37 || value == 50) {
        printf(" -----------\n");
    printf("| J %c       |\n",couleur);
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|        J %c|\n",couleur);
    printf(" -----------\n\n");
    } else if (value == 12 || value == 25 || value == 38 || value == 51) {
        printf(" -----------\n");
    printf("| Q %c       |\n",couleur);
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|        Q %c|\n",couleur);
    printf(" -----------\n\n");
    } else if (value == 13 || value == 26 || value == 39 || value == 52) {
        printf(" -----------\n");
    printf("| K %c       |\n",couleur);
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|           |\n");
    printf("|        K %c|\n",couleur);
    printf(" -----------\n\n");
    } else {
        printf(" ------------\n");
    printf("|%2d %c        |\n",real_value(value),couleur);
    printf("|            |\n");
    printf("|            |\n");
    printf("|            |\n");
    printf("|            |\n");
    printf("|            |\n");
    printf("|            |\n");
    printf("|        %2d %c|\n",real_value(value),couleur);
    printf(" ------------\n\n");
    }
   
}



int main() {
    int i = 0, card_acc, acc = 2, init_dsum;
    int res_p1, sum1 = 0;
    int res_p2, sum2 = 0;
    

    //les règles sont précisées ci-dessous
    printf("Welcome to BLACKJACK !\n");
    waitUser();

    printf(" -> Your goal is to score exactly 21 points or the nearest possible to 21.\n");
    sleep(1);
    printf(" -> You cannot exceed 21 points.\n");
    sleep(1);
    printf(" -> You must have a final score greater than the dealer's one.\n");
    sleep(1);
    printf(" -> If the dealer's final score is greater than 21, everyone who has a score less or equal to 21 wins the game.\n");
    sleep(1);
    printf(" -> As' cards are equal to 1 point in every situation.\n\n");
    sleep(1);
    waitUser();
    clearUserScreen();

    //le jeu commence ici
    printf("The game begins in...\n");
    printf("3\n");
    sleep(1);
    printf("2\n");
    sleep(1);
    printf("1\n");
    sleep(1);
    clearUserScreen();

    //les 2 premières cartes du croupier sont ici
    srand(time(NULL));
    dealer[0] = (rand() % 52) + 1;
    
    dealer[1] = (rand() % 52) + 1;
    
    cards[dealer[0]-1] = 0;
    cards[dealer[1]-1] = 0;

    printf("-- DEALER TURNS HIS CARDS UP --\n\n");
    sleep(2);

    printf("Dealer first card is : \n\n");
    sleep(1);
    draw_card(dealer[0],real_color(dealer[0]));
    sleep(1);
    printf("Dealer second card is : hidden card\n\n");
    sleep(1);
    waitUser();
    clearUserScreen();

    //les 2 premières cartes de chaque joueur sont ici
    printf("Each player will now receive their two first initial cards :\n\n");
    sleep(2);

    
    player1[0] = (rand() % 52) + 1;
    if (cards[player1[0]-1] == 0) {
        while (cards[player1[0]-1] == 0) {
            player1[0] = (rand() % 52) + 1;
        }
        cards[player1[0]-1] = 0;
    } else {
        cards[player1[0]-1] = 0;
    }

    
    player1[1] = (rand() % 52) + 1;
    if (cards[player1[1]-1] == 0) {
        while (cards[player1[1]-1] == 0) {
            player1[1] = (rand() % 52) + 1;
        }
        cards[player1[1]-1] = 0;
    } else {
        cards[player1[1]-1] = 0;
    }

    
    player2[0] = (rand() % 52) + 1;
    if (cards[player2[0]-1] == 0) {
        while (cards[player2[0]-1] == 0) {
            player2[0] = (rand() % 52) + 1;
        }
        cards[player2[0]-1] = 0;
    } else {
        cards[player2[0]-1] = 0;
    }

    
    player2[1] = (rand() % 52) + 1;
    if (cards[player2[1]-1] == 0) {
        while (cards[player2[1]-1] == 0) {
            player2[1] = (rand() % 52) + 1;
        }
        cards[player2[1]-1] = 0;
    } else {
        cards[player2[1]-1] = 0;
    }

    printf("Player 1 first cards are : \n\n");
    sleep(1);
    draw_card(player1[0],real_color(player1[0]));
    sleep(1);
    draw_card(player1[1],real_color(player1[1]));
    sleep(1);
    printf("Player 2 first cards are : \n\n");
    sleep(1);
    draw_card(player2[0],real_color(player2[0]));
    sleep(1);
    draw_card(player2[1],real_color(player2[1]));

    waitUser();
    clearUserScreen();

    //ici, le joueur 1 peut tirer d'autres cartes ou continuer avec sa main initiale
    printf("Player 1, you can now choose either to turn more cards up or to keep your current values :\n\n");
    sleep(2);
    
    printf("Your current cards are : \n\n");
    draw_card(player1[0],real_color(player1[0]));
    draw_card(player1[1],real_color(player1[1]));
    printf("Would you like to turn up more cards ? Enter '1' for YES or '2' for NO : \n");
    scanf("%d", &res_p1);
    if (res_p1 == 1) {
        while(res_p1 == 1) {
            card_acc = (rand() % 52) + 1;
            if (cards[card_acc-1] == 0) {
                while(cards[card_acc-1] == 0) {
                    card_acc = (rand() % 52) + 1;
                }
                cards[card_acc-1] = 0;
            } else {
                cards[card_acc-1] = 0;
            }
            player1[acc] = card_acc;
            ++acc;

            while (player1[i] != 0) {
                sum1 += real_value(player1[i]);
                ++i;
            }

            printf("\n");
            printf("Your next card is : \n\n");
            sleep(1);
            draw_card(card_acc,real_color(card_acc));
            sleep(1);

            if (victory(sum1) == 1) {
                printf("->     Your final cards are :     <-\n\n");
                for (int a = 0; player1[a] != 0; ++a) {
                    draw_card(player1[a],real_color(player1[a]));
                }
            printf("\n");
            printf("Your final score is %d, which is greater than 21. You've lost the game !\n",sum1);
            sum1 = 0;
            break;
            } else {
                printf("Your current score is : %d\n",sum1);
                printf("Would you like to turn up more cards ? Enter '1' for YES or '2' for NO : \n");
                scanf("%d", &res_p1);
            }
        }
        if (sum1 != 0) {
            printf("\n");
            printf("You decided to stop turning up new cards. Your final score is : %d\n",sum1);
        }
        
    } else {
        while (player1[i] != 0) {
                sum1 += real_value(player1[i]);
                ++i;
                }
        printf("\n");
        printf("You decided to keep your current values. You can no longer have more cards. Your final score is : %d\n", sum1);
    }
    if(isWindows() == 1) getchar();
    waitUser();
    clearUserScreen();

    //ré-initialisation de quelques variables
    i = 0;
    acc = 2;

    //ici, le joueur 2 peut tirer d'autres cartes ou continuer avec sa main initiale
    printf("Player 2, you can now choose either to turn more cards up or to keep your current values :\n\n");
    sleep(1);
    
    printf("Your current cards are : \n\n");
    draw_card(player2[0],real_color(player2[0]));
    draw_card(player2[1],real_color(player2[1]));
    printf("Would you like to turn up more cards ? Enter '1' for YES or '2' for NO : \n");
    scanf("%d", &res_p2);
    if (res_p2 == 1) {
        while(res_p2 == 1) {
            card_acc = (rand() % 52) + 1;
            if (cards[card_acc-1] == 0) {
                while(cards[card_acc-1] == 0) {
                    card_acc = (rand() % 52) + 1;
                }
                cards[card_acc-1] = 0;
            } else {
                cards[card_acc-1] = 0;
            }
            player2[acc] = card_acc;
            ++acc;

            while (player2[i] != 0) {
                sum2 += real_value(player2[i]);
                ++i;
            }

            printf("\n");
            printf("Your next card is : \n\n");
            sleep(1);
            draw_card(card_acc,real_color(card_acc));
            sleep(1);

            if (victory(sum2) == 1) {
                printf("->     Your final cards are :     <-\n\n");
                for (int b = 0; player2[b] != 0; ++b) {
                    draw_card(player2[b],real_color(player2[b]));
                }
            printf("\n");
            printf("Your final score is %d, which is greater than 21. You've lost the game !\n",sum2);
            sum2 = 0;
            break;
            } else {
                printf("Your current score is : %d\n",sum2);
                printf("Would you like to turn up more cards ? Enter '1' for YES or '2' for NO : \n");
                scanf("%d", &res_p2);
            }
        }
        if (sum2 != 0) {
            printf("\n");
            printf("You decided to stop turning up new cards. Your final score is : %d\n",sum2);
        }

    } else {
        while (player2[i] != 0) {
                sum2 += real_value(player2[i]);
                ++i;
                }
        printf("\n");
        printf("You decided to keep your current values. You can no longer have more cards. Your final score is : %d\n", sum2);
    }
    if(isWindows() == 1) getchar();
    waitUser();
    clearUserScreen();

    acc = 2;

    //on a le tour du croupier ici
    //si sa main est <= 16, il va tirer d'autres cartes jusqu'au moment où il aura > 16 points
    //s'il dépasse 21 points, tout le monde qui n'a pas fait plus de 21 points antérieurement gagne 
    printf("-- THIS IS THE DEALER'S TURN --\n\n");
    sleep(2);

    init_dsum = real_value(dealer[0]) + real_value(dealer[1]);
    printf("Dealer current cards are : \n\n");
    sleep(1);
    draw_card(dealer[0],real_color(dealer[0]));
    sleep(1);
    draw_card(dealer[1],real_color(dealer[1]));
    sleep(2);

    if (init_dsum < 16) {
        printf("Dealer's score is less than 16. He will now turn up as much as necessary cards until he gets a score greater than 16.\n\n");
        sleep(2);
        while (init_dsum < 16) {
        init_dsum = 0;
        card_acc = (rand() % 52) + 1;
        if (cards[card_acc-1] == 0) {
            while(cards[card_acc-1] == 0) {
                card_acc = (rand() % 52) + 1;
                }
                cards[card_acc-1] = 0;
        } else {
            cards[card_acc-1] = 0;
        }
        dealer[acc] = card_acc;
        ++acc;
        for (int c = 0; dealer[c] != 0; ++c) {
            init_dsum += real_value(dealer[c]);
        }
      }
      printf("Dealer's hand is : \n\n");
      draw_card(dealer[0],real_color(dealer[0]));
      draw_card(dealer[1],real_color(dealer[1]));
      sleep(1);
      for (int d = 2; dealer[d] != 0; ++d) {
          draw_card(dealer[d],real_color(dealer[d]));
          sleep(1);
      }
      printf("\n");
      sleep(1);
      printf("Dealer's final score is : %d\n\n",init_dsum);
      sleep(1);

      if (init_dsum > 21) {
          printf("NO WAY ! Dealer's score is greater than 21 ! Everyone who has a final score less than 21 wins the game !\n\n");
          sleep(1);
          if (sum1 != 0) {
              printf("Player 1 wins the game! -- BLACKJACK --\n");
          } else {
              printf("Player 1 loses the game!\n\n");
          }
          if (sum2 != 0) {
              printf("Player 2 wins the game! -- BLACKJACK --\n");
          } else {
              printf("Player 2 loses the game!\n\n");
          }
      } else {
          printf("You win the game if you have a final score greater than %d.\n\n",init_dsum);
          sleep(2);
          if (sum1 != 0 && sum1 > init_dsum) {
              printf("Player 1 wins the game with %d points! -> BLACKJACK <-\n",sum1);
          } else {
              printf("Player 1 final score is less than dealer's score!\n");
              printf("Player 1 loses the game!\n\n");
          }
          if (sum2 != 0 && sum2 > init_dsum) {
              printf("Player 2 wins the game with %d points! -> BLACKJACK <-\n",sum2);
          } else {
              printf("Player 2 final score is less than dealer's score!\n");
              printf("Player 2 loses the game!\n\n");
          }
      }
    } else {
        printf("You win the game if you have a final score greater than %d.\n\n",init_dsum);
        sleep(2);
        if (sum1 != 0 && sum1 > init_dsum) {
              printf("Player 1 wins the game with %d points! -> BLACKJACK <-\n\n",sum1);
          } else if (sum1 > 0) {
              printf("Player 1 final score is less than dealer's score!\n");
              printf("Player 1 loses the game!\n\n");
          } else {
              printf("Player 1 final score is greater than 21!\n");
              printf("Player 1 loses the game!\n\n");
          }
        if (sum2 != 0 && sum2 > init_dsum) {
            printf("Player 2 wins the game with %d points! -> BLACKJACK <-\n\n",sum2);
          } else if (sum2 > 0) {
              printf("Player 2 final score is less than dealer's score!\n");
              printf("Player 2 loses the game!\n\n");
          } else {
              printf("Player 2 final score is greater than 21!\n");
              printf("Player 2 loses the game!\n\n");
          }
    }

    waitUser();

    printf("-- PROJET DE FIN DE SEMESTRE --\n\n");
    printf("Université Paris 8\n");
    printf("Mars - Avril 2021\n");
    printf("L1-Z Informatique -- Département STN\n");
    printf("Anri Kennel - Felipe Pereira Manfrin - Kévin Martins Da Veiga - William Nizet\n\n");

    printf("Ce projet a été développé dans le cadre de l'UE 'Outils informatiques collaboratifs' avec Mme. Françoise Balmas");

    waitUser();
    
    return 0;
}
