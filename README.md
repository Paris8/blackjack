# Informations

Université Paris 8
Projet de fin de semestre en C - Blackjack
Outils informatiques collaboratifs - Mme. BALMAS Françoise
L1Z - Informatique

# Élèves

| NOM Prénom               | Numéro étudiant   |
| -----------------------: |:-----------------:|
| KENNEL Anri              | 20010664          |
| MARTINS DA VEIGA Kévin   | 20009472          |
| NIZET William            | 20005517          |
| PEREIRA MANFRIN Felipe   | 20034559          |


# Autres

## Dépendances linux

Lancez `sudo apt-get install libncurses5-dev libncursesw5-dev` pour installer la librarie curses.h sur Linux.

## Lien vers le repo

https://code.up8.edu/felipepm/blackjack
