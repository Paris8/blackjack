CC = gcc
CFLAGS = -Wall -Wextra
ALL = blackjack
CLEAN = rm -f *.o

all: $(ALL)

blackjack: blackjack.o
	$(CC) $^ -o blackjack -lncurses
	$(CLEAN)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	$(CLEAN) $(ALL)
